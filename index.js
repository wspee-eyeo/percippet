
function handleForm(form) {
  let image = form.querySelector('#image');
  let handler = (ev) => {
      let reader = new FileReader();
      reader.onload = (ev) => {
        let imageElement = new Image();
        imageElement.onload = () => {
          let canvas = document.querySelector('#canvas');

          let blockBits = parseInt(
            document.querySelector('#blockBits').value, 10);
          let selection =
            (document.querySelector('#selection').value || '').split('x');

          let sX = selection[0] || 0;
          let sY = selection[1] || 0;
          let sWidth = selection[2] || imageElement.width;
          let sHeight = selection[3] || imageElement.height;

          canvas.width = sWidth;
          canvas.height = sHeight;

          let context = canvas.getContext('2d');
          context.drawImage(
            imageElement, sX, sY, sWidth, sHeight, 0, 0, sWidth, sHeight);

          let imageData = context.getImageData(0, 0, sWidth, sHeight);
          let result = form.querySelector('#result');

          result.value = blockhashjs.blockhashData(imageData, blockBits, 2);
        };
        imageElement.src = ev.target.result;
      };
      reader.readAsDataURL(ev.target.files[0]);
  };
  image.addEventListener('change', handler , false);
}
handleForm(document.querySelectorAll('form')[0]);
